/*
 * Copyright (C) 2018
 *     Biotector Analytical Systems Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <UC1701.h>
#include <ModbusMaster.h>
#include <Ethernet2.h>
#include <SPI.h>
#include <Wire.h>

enum btn_t {
	BTN_NONE,
	BTN_UP,
	BTN_DOWN,
	BTN_ENTER,
	NUM_BTNS
};

enum screen_t {
	SCREEN_MENU,
	SCREEN_RTU,
	SCREEN_TCP,
	SCREEN_RTU_STRESS,
	SCREEN_TCP_STRESS,
	NUM_SCREENS
};

enum test_t {
	TEST_RTU,
	TEST_TCP,
	TEST_RTU_STRESS,
	TEST_TCP_STRESS,
	NUM_TESTS
};

struct ip_addr_t               //ip address struct
{
	uint8_t         oct0;          //octet 0
	uint8_t         oct1;          //octet 1
	uint8_t         oct2;          //octet 2
	uint8_t         oct3;          //octet 3
};

struct config_t                //configuration struct
{
	uint32_t        baud;          //rs485 baud rate
	uint32_t        port;          //tcp port
	ip_addr_t       ip_addr;       //device ip address
	ip_addr_t       ip_server;     //modbus tcp server to test
};

const unsigned RS485_DE       = 9;            //rs485 data enable
const unsigned RS485_RE_NEG   = 2;            //rs485 receiver enable
const unsigned LCD_NUM_ROWS   = 8;
const unsigned LCD_CHAR_WIDTH = 5+1;          //character width in pixels
const unsigned NUM_TEST_RUNS  = 10;
const unsigned BACKLIGHT_PIN  = 26;
const unsigned SLAVE_ID       = 7;
const unsigned EEPROM_RTC     = 0x57;

//global variables
uint32_t  btn_press_tm = 0;    //last time a button was pressed
unsigned  screen_index = 0;    //current screen in menu
unsigned  cursor_row;          //cursor row in non-info menus
btn_t     last_btn_press;      //the last pressed button (or none)
byte      mac[6];              //ethernet mac address

test_t         test_type;
unsigned       num_tests;      //number of tests to run
unsigned       test_errors;    //error counter
unsigned       stress_tests;   //stress test counter
unsigned       rate_val;       //measured rate of messages per second
unsigned       rate_count;     //count of messages in the current second
unsigned       rate_start;     //start time of current measurement second

UC1701         lcd;            //lcd screen
config_t       config;         //configuration
ModbusMaster   node;
EthernetClient client;


/*
 * Read a byte from I2C EEPROM
 *     dev: device address
 *     reg: register address
 */
uint8_t eepromReadByte(uint8_t dev, uint8_t reg)
{
	delay(5);

	Wire.beginTransmission(dev);
	Wire.write(reg);
	Wire.endTransmission();

	Wire.requestFrom(dev, 1);

	return Wire.read();
}

/*
 * Prepare the RS485 port to transmit data
 */
void preTransmit()
{
	digitalWrite(RS485_RE_NEG, 1);
	digitalWrite(RS485_DE,     1);
}

/*
 * Prepare the RS485 port to receive data
 */
void postTransmit()
{
	digitalWrite(RS485_RE_NEG, 0);
	digitalWrite(RS485_DE,     0);
}

/*
 * Setup the RS485 port
 */
void setupRS485()
{
	pinMode(RS485_RE_NEG, OUTPUT);
	pinMode(RS485_DE,     OUTPUT);

	digitalWrite(RS485_RE_NEG, 0);
	digitalWrite(RS485_DE,     0);

	Serial.begin(57600);
	node.begin(SLAVE_ID, Serial);

	node.preTransmission(preTransmit);
	node.postTransmission(postTransmit);
}

/*
 * Returns true if the user has not pressed a button within the timeout
 *     timeout: timeout value in seconds
 */
bool isUserTimeout(unsigned timeout)
{
	return millis() - btn_press_tm > timeout * 1000;
}

/*
 * Setup the user-interface buttons
 */
void setupButtons()
{
	attachInterrupt(24, enterPressed, FALLING);
	attachInterrupt(25, upPressed,    FALLING);
	attachInterrupt(23, downPressed,  FALLING);
}

/*
 * Return the last pressed button
 */
btn_t getButtonPress()
{
	btn_t btn = last_btn_press;

	last_btn_press = BTN_NONE;

	return btn;
}

/*
 * Record a button press
 *
 * Limits button presses to one per 150ms
 */
void recordButtonPress(btn_t btn)
{
	//throttle button presses
	if( millis() - btn_press_tm > 150 )
	{
		last_btn_press = btn;
		btn_press_tm   = millis();
	}
}

/*
 * Record an Enter button press
 */
void enterPressed()
{
	recordButtonPress(BTN_ENTER);
}

/*
 * Record a Down button press
 */
void downPressed()
{
	recordButtonPress(BTN_DOWN);
}

/*
 * Record an Up button press
 */
void upPressed()
{
	recordButtonPress(BTN_UP);
}

/*
 * Converts our IP address object into the format used by the
 * Arduino Ethernet library
 */
IPAddress getIpAddr(ip_addr_t& addr)
{
	return IPAddress(addr.oct0, addr.oct1, addr.oct2, addr.oct3);
}

/*
 * Load the MAC address from the RTC EEPROM
 * We use the last 6 bytes of the 8 byte UI-64 MAC in the MCP79402 RTC,
 * registers 0xF0 to 0xF7
 */
void loadMAC()
{
	for(int i=0; i<6; i++)
		mac[i] = eepromReadByte(EEPROM_RTC, 0xF2 + i);
}

/*
 * Setup the Ethernet port
 */
void setupEthernet()
{
	loadMAC();

	IPAddress addr = getIpAddr(config.ip_addr);

	Ethernet.begin(mac, addr);
}

/*
 * Setup the LCD backlight
 */
void setupBacklight()
{
	pinMode(BACKLIGHT_PIN, OUTPUT);
}

/*
 * Manage the LCD backlight
 */
void manageBacklight()
{
	bool bl_enabled = true;

	if( isUserTimeout(600) )
		bl_enabled = false;

	digitalWrite(BACKLIGHT_PIN, bl_enabled);
}

/*
 * Sets the default configuration
 */
void setDefaultConfig()
{
	config.port       = 502;
	config.baud       = 57600;
	config.ip_addr    = { 192, 168, 254, 100 };
	config.ip_server  = { 192, 168, 254, 254 };
}

/*
 * Setup the LCD screen
 */
void setupLCD()
{
	lcd.begin();

	cursor_row     = 0;
	last_btn_press = BTN_NONE;
}

/*
 * Setup the modbus-test variables
 */
void initTestVars(unsigned tests)
{
	num_tests    = tests;
	test_errors  = 0;

	stress_tests = 0;
	rate_val     = 0;
	rate_count   = 0;
	rate_start   = millis();
}

/*
 * Setup a modbus-test of the passed type
 */
void setupTest(test_t type)
{
	if( test_type < NUM_TESTS )
	{
		test_type = type;

		initTestVars(NUM_TEST_RUNS);
	}
}

/*
 * Stop any running modbus-test
 */
void stopTest()
{
	num_tests = 0;
}

/*
 * Setup the I2C bus
 */
void setupI2C()
{
	Wire.begin();
}

/*
 * Show the main menu
 */
void showScreenMenu()
{
	lcd.setCursor(0, 0);
	lcd.print("Modbus Tester");

	auto col = 1 * LCD_CHAR_WIDTH;

	lcd.setCursor(col, 2);
	lcd.print("RTU Test");

	lcd.setCursor(col, 3);
	lcd.print("TCP Test");

	lcd.setCursor(col, 4);
	lcd.print("RTU Stress Test");

	lcd.setCursor(col, 5);
	lcd.print("TCP Stress Test");

	//draw cursor
	unsigned row_offset = 2;
	for(unsigned i=0; i<LCD_NUM_ROWS; i++)
	{
		char c = ' ';

		if( cursor_row == i )
			c = '>';

		lcd.setCursor(0, i+row_offset);
		lcd.print(c);
	}

	switch( getButtonPress() )
	{
	case BTN_UP:
		if( cursor_row > 0 )
			cursor_row--;
		break;
	case BTN_DOWN:
		if( cursor_row < 3 )
			cursor_row++;
		break;
	case BTN_ENTER:
		switch( cursor_row )
		{
		case 0:
			screen_index = SCREEN_RTU;
			setupTest(TEST_RTU);
			break;
		case 1:
			screen_index = SCREEN_TCP;
			setupTest(TEST_TCP);
			break;
		case 2:
			screen_index = SCREEN_RTU_STRESS;
			setupTest(TEST_RTU_STRESS);
			break;
		default:
			screen_index = SCREEN_TCP_STRESS;
			setupTest(TEST_TCP_STRESS);
			break;
		}
		break;
	case NUM_BTNS:
	case BTN_NONE:
		break;
	}
}

/*
 * Print the passed ip address to the passed screen handle
 */
void printIp(UC1701& lcd, ip_addr_t& ip)
{
	lcd.print(ip.oct0);
	lcd.print(".");
	lcd.print(ip.oct1);
	lcd.print(".");
	lcd.print(ip.oct2);
	lcd.print(".");
	lcd.print(ip.oct3);
}

/*
 * Present the test result (PASS/FAIL) and wait for user to acknowledge
 */
void showTestStatus()
{
	//show test result
	lcd.clear();
	lcd.setCursor(20, 2);
	if( test_errors )
		lcd.print("TEST FAILED");
	else
		lcd.print("TEST PASSED");

	lcd.setCursor(20, 4);
	lcd.print("Press Enter");

	//clear any old button presses
	getButtonPress();

	//wait for user to acknowledge
	while( getButtonPress() != BTN_ENTER )
		delay(100);

	screen_index = SCREEN_MENU;
}

/*
 * Show Modbus RTU test screen
 */
void showScreenRTU()
{
	lcd.setCursor(0, 0);
	lcd.print("Modbus RTU Test");

	lcd.setCursor(0, 2);
	lcd.print("ID:   ");
	lcd.print(SLAVE_ID);

	lcd.setCursor(0, 3);
	lcd.print("Baud: 57600");

	lcd.setCursor(0, 5);
	lcd.print("Test: ");
	lcd.print(NUM_TEST_RUNS - num_tests);

	lcd.setCursor(0, 6);
	lcd.print("Errors: ");
	lcd.print(test_errors);

	if( num_tests == 0 )
		showTestStatus();
}

/*
 * Show Modbus TCP test screen
 */
void showScreenTCP()
{
	lcd.setCursor(0, 0);
	lcd.print("Modbus TCP Test");

	lcd.setCursor(0, 2);
	lcd.print("ID: ");
	lcd.print(SLAVE_ID);

	lcd.setCursor(0, 3);
	lcd.print("Addr: ");
	printIp(lcd, config.ip_server);

	lcd.setCursor(0, 5);
	lcd.print("Test: ");
	lcd.print(NUM_TEST_RUNS - num_tests);

	lcd.setCursor(0, 6);
	lcd.print("Errors: ");
	lcd.print(test_errors);

	if( num_tests == 0 )
		showTestStatus();
}

/*
 * Show Modbus RTU Stress test screen
 */
void showScreenStressRTU()
{
	lcd.setCursor(0, 0);
	lcd.print("RTU Stress Test");

	lcd.setCursor(0, 2);
	lcd.print("ID:   ");
	lcd.print(SLAVE_ID);

	lcd.setCursor(0, 3);
	lcd.print("Baud: 57600");

	lcd.setCursor(0, 4);
	lcd.print("Rate: ");
	lcd.print(rate_val);
	lcd.print(" msg/s");
	//ensure we clear old chars if rate reduces
	lcd.print("   ");

	lcd.setCursor(0, 6);
	lcd.print("Errors: ");
	lcd.print(test_errors);
	lcd.print("/");
	lcd.print(stress_tests);

	//button press stops test
	if( getButtonPress() == BTN_ENTER )
	{
		stopTest();
		screen_index = SCREEN_MENU;
	}
}

/*
 * Show Modbus TCP Stress test screen
 */
void showScreenStressTCP()
{
	lcd.setCursor(0, 0);
	lcd.print("TCP Stress Test");

	lcd.setCursor(0, 2);
	lcd.print("ID:   ");
	lcd.print(SLAVE_ID);

	lcd.setCursor(0, 3);
	lcd.print("Addr: ");
	printIp(lcd, config.ip_server);

	lcd.setCursor(0, 4);
	lcd.print("Rate: ");
	lcd.print(rate_val);
	lcd.print(" msg/s");
	//ensure we clear old chars if rate reduces
	lcd.print("   ");

	lcd.setCursor(0, 6);
	lcd.print("Errors: ");
	lcd.print(test_errors);
	lcd.print("/");
	lcd.print(stress_tests);

	//button press stops test
	if( getButtonPress() == BTN_ENTER )
	{
		stopTest();
		screen_index = SCREEN_MENU;
	}
}

/*
 * Manage the user-interface
 */
void manageUI()
{
	//clear screen if changing menus
	static unsigned my_index;
	if( my_index != screen_index )
	{
		lcd.clear();
		my_index = screen_index;
	}

	switch( screen_index )
	{
	case SCREEN_RTU:
		showScreenRTU();
		break;
	case SCREEN_TCP:
		showScreenTCP();
		break;
	case SCREEN_RTU_STRESS:
		showScreenStressRTU();
		break;
	case SCREEN_TCP_STRESS:
		showScreenStressTCP();
		break;
	case SCREEN_MENU:
	case NUM_SCREENS:
	default:
		showScreenMenu();
	}
}

/*
 * Run a modbus test if one is setup
 */
void tryRunModbusTest()
{
	if( num_tests == 0 )
	{
		delay(100);
		return;
	}

	stress_tests++;

	uint16_t val;
	uint8_t  status = -1;

	if( test_type == TEST_RTU || test_type == TEST_RTU_STRESS )
	{
		unsigned reg = 40500; //device address register

		unsigned offset = reg - 40001;

		status = node.readHoldingRegisters(offset, 1);

		if( status == 0 )
		{
			val = node.getResponseBuffer(0);

			if( val != SLAVE_ID )
				status = -1;
		}
	}

	if( test_type == TEST_TCP || test_type == TEST_TCP_STRESS )
	{
		auto dest_ip = getIpAddr(config.ip_server);

		int connected = client.connect(dest_ip, config.port);
		if( connected )
		{
			//send the request: header + 0x07 03 01 F3 00 01
			uint8_t req[] = { 0x0, 0x1, 0x0, 0x0, 0x0, 0x6, 0x7, 0x3, 0x01, 0xF3, 0x0, 0x1 };

			client.write(req, 12);

			//wait for the response
			unsigned long start = millis();
			while( millis() - start < 500 )
			{
				if( client.available() )
					break;
				delay(1);
			}

			//read response
			unsigned num_bytes = 0;
			unsigned buf_size  = 50;
			uint8_t buf[buf_size];
			while( client.available() )
			{
				buf[num_bytes] = client.read();

				num_bytes++;

				if( num_bytes >= buf_size )
					break;

				if( ! client.available() )
					delay(1);
			}

			client.stop();

			//check response
			if( num_bytes == 11 )
			{
				status = 0;

				if( buf[6] != 0x7 )
					status = -1;
				if( buf[7] != 0x3 )
					status = -2;
			}
		}
	}

	if( status != 0 )
		test_errors++;

	//keep track of message rate
	if( status == 0 )
		rate_count++;
	unsigned ms = millis();
	if( ms - rate_start > 1000 )
	{
		rate_start = ms;
		rate_val   = rate_count;
		rate_count = 0;
	}

	if( test_type == TEST_RTU || test_type == TEST_TCP )
	{
		num_tests--;
		delay(500);
	}
}

/*
 * Setup the hardware and global variables
 */
void setup()
{
	initTestVars(0);

	setupLCD();

	setupButtons();

	setupBacklight();

	setDefaultConfig();

	setupRS485();

	setupI2C();

	setupEthernet();
}

/*
 * main loop - handle modbus tests and run the UI
 */
void loop()
{
	tryRunModbusTest();

	manageBacklight();

	manageUI();
}
