This project configures an Industruino D21G PLC with Ethernet expansion module
as a modbus test device for Biotector analysers.

A menu is presented to the user which allows them to choose a Modbus RTU test or
a modbus TCP test. On selecting a test the machine will read the holding register
DEVICE_ADDR (40500) ten times and check the result each time. If the value read
is correct the the screen will show a TEST PASSED message or on failure a
TEST FAILED message.

Notes:
	1. The tests assume the analyser uses modbus bus address 7
	2. The TCP tests assume the slave uses address 192.168.254.254
	3. The Ethernet 2 library has been tweaked (speed reduced from 8mhz to 
	   4mhz) as required for industruino use.

Changes:
	Rev 1. a81b024054827593f3a567d1a3c66f50c8c37a9b
		Initial release.
